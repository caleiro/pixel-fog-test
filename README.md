# Fog Test
A front-end layout orientated challenge designed for front-end candidates.

### Task
- Build the layout based on the **design** and **specifications** below using only a single **index.html** and single **index.css**
- Download this repository to use the `index` folder as a template

### Submission
1. Create a **private repository** using Github, Bitbucket or Gitlab and give [info@pixelfusion.co.nz](mailto:info@pixelfusion.co.nz) access
2. Or alternatively you can send us a ZIP

### Specifications
- Use a mobile first approach
- Liquid layout (Make sure the design responds between the desktop and mobile screen widths)
- Make the title of the `index.html` your full name and time taken e.g *"Aron Ralston: 127hrs"*
- Browser support: Google Chrome Latest (v58)
- Comment your code where necessary e.g *"I am using this technique for this reason"*

### Design
- [Mobile (400px)](https://raw.githubusercontent.com/pixelfusion/fog-test/master/design/mobile.png)
- [Desktop (1200px)](https://raw.githubusercontent.com/pixelfusion/fog-test/master/design/desktop.png)
- [Assets](https://github.com/pixelfusion/fog-test/blob/master/index/assets)
- Font: Helvetica Neue (Regular, Bold)
